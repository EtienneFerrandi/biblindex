# BiblIndex [![Symfony](https://github.com/janalis/biblindex/actions/workflows/symfony.yml/badge.svg)](https://github.com/janalis/biblindex/actions/workflows/symfony.yml)

## Maintainers

| Name              | Email               |
|-------------------|---------------------|
| Pierre Hennequart | pierre@janalis.com  |
| Etienne Ferrandi  | etienne@janalis.com |

## Clone the GIT project
```shell
$ git clone git@github.com:janalis/biblindex.git biblindex
```

## Create the `.env` file
From the `.env` file, create the `.env.local` file

```shell
APP_DEFAULT_URI=https://127.0.0.1:8000
APP_EMAIL_CC=
APP_EMAIL_BCC=
APP_EMAIL_FROM=youremail@example.com
APP_EMAIL_TO=youremail@example.com
DATABASE_URL=mysql://username@localhost:3306/biblindex
DATABASE_VERSION=mariadb-10.**
MAILER_DSN=smtp://localhost
```

## Download the database
```shell
$ scp biblindex@cchum-kvm-biblindex.in2p3.fr:/data/backup/mysql/biblindex-DD-MMM.-YYYY.tar.gz .
	##DD## : Jour avec le zéro si nécessaire
	##MM## : Mois avec 3 ou 4 lettres à la française, suivi d'un point
	#YYYY## : Année en 4 chiffres
```
Unarchive the database dump and import it into mysql:
> mysql -u username -ppassword -h localhost biblindex < dump.sql

## Download uploads
```shell
$ scp biblindex@cchum-kvm-biblindex.in2p3.fr:/data/backup/files/biblindex-files-##DD##-##MM##-##YYYY##.tar.gz
    ##DD## : Day with zero if necessary
    ##MM## : Month with 3 or 4 French letters, followed by a dot
    #YYYY## : Year in 4 digits
```
In the archive, navigate to the **uploads** folder, and drop these files into the `public/uploads` folder at the root of the project.

## Install backend dependencies
```shell
$ composer install
```

## Install frontend dependencies
```shell
$ nvm use
$ yarn
$ yarn build
```

## Start the server
```shell
$ symfony server:start
```

## Access to the application
> https://localhost:8000/

## Access to the administration interface
> https://localhost:8000/en/admin/
